define(function (require) {
	'use strict';

	/*
	* Override Validation Methods with plugins
	* */
	var Core = require('plugins/core');

	return {

		//General-use
		"AlwaysTrue": function () {
			return Core.AlwaysTrue();
		},
		"AlwaysFalse": function () {
			//Useful for cases where requested constraint is not valid
			return Core.AlwaysFalse();
		},
		"Boolean": function(inputToValidate) {
			return Core.Boolean(inputToValidate);
		},
		"Length": function (inputToValidate, requiredLength) {
			//@TODO: Make this constraint
			return Core.Length(inputToValidate, requiredLength);
		},
		"Required": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"NotBlank": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"NotEmpty": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"Blank": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"Empty": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"Pattern": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"Matches": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"MinCaps": function (input, minCaps) {
			return Core.MinCaps(input, minCaps);
		},
		"MinLen": function (input, minLen) {
			return Core.MinLen(input, minLen);
		},





		//Number-based
		"Numeric": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"Digits": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"Integer": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"Real": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"Currency": function() {
			//@TODO: Make this constraint
			return true;
		},
		"Max": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"Min": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"Range": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"Between": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},



		//String-based
		"URL": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"Email": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"Alpha": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"Alphanumeric": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},



		//Date-based
		"Past": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},
		"Future": function (inputToValidate) {
			//@TODO: Make this constraint
			return true;
		},


		//Finance-related
		"RoutingNumber": function (inputToValidate) {
			return Core.RoutingNumber(inputToValidate);
		}
	};
});
