define({
	"AccountMask": {
		"constraints": [
			"Text"
		]
	},
	"AccountNumber": {
		"constraints": [
			"Integer",
			"MinLen 8",
			"MaxLen 8"
		]
	},
	"AccountNickname": {
		"constraints": [
			"Text"
		]
	},
	"Description": {
		"constraints": [
			"Text",
			"MinLen 1"
		]
	},
	"Category": {
		"constraints": [
			"Text",
			"MinLen 2"
		]
	},
	"Currency": {
		"constraints": [
			"Decimal",
			"Scale 2"
		]
	},
	"Date": {
		"constraints": [
			"Date"
		]
	},
	"DateTime": {
		"constraints": [
			"DateTime"
		]
	},
	"Email": {
		"constraints": [
			"Email"
		]
	},
	"Id": {
		"constraints": [
			"Alphanumeric",
			"MinLen 6",
			"MinNums 1",
			"Required"
		]
	},
	"Image": {
		"constraints": [
			"AlwaysTrue"
		]
	},
	"List": {
		"constraints": [
			null
		]
	},
	"Money": {
		"constraints": [
			"Number"
		]
	},
	"Number": {
		"constraints": [
			"Integer",
			"Min 1"
		]
	},
	"Password": {
		"constraints": [
			"MinLen 1",
			"MaxLen 100",
			"MinNums 1",
			"MinSpecial 1",
			"MinCaps 1"
		]
	},
	"Payee": {
		"constraints": [
			"Text",
			"MaxLen 30"
		]
	},
	"Phone": {
		"constraints": [
			"Number",
			"MinLen 10"
		]
	},
	"RoutingNumber": {
		"constraints": [
			"RoutingNumber"
		]
	},
	"Text": {
		"constraints": [
			"Text",
			"MinLen 1"
		]
	},
	"OnOff": {
		"constraints": [
			"Boolean"
		]
	}
});
