define(function (require) {

	'use strict';

	var defaultConstraints = require('constraints/default'),
		semanticContraints = require('constraints/semantic'),
		validate = {},
		$validate = this;

	this.validateConstraint = function (input, constraint) {

		var validationMethod,
			validators = defaultConstraints,
			constraintAsList = !constraint instanceof Array ? constraint.split(" ") : [constraint],
			evalMethod = constraintAsList[0];

		//Remove the method form the list
		constraintAsList.shift();

		//Make list of params
		var paramsExist = (constraintAsList instanceof Array && constraintAsList.length > 0),
			evalMethodParams = paramsExist ? constraintAsList : null,
			finalParamsToApply = [];

		//Lookup the requested evaluation method and set as the validator of choice
		validationMethod = validators[evalMethod] || validators.AlwaysTrue;


		//Prepare to run the validator
		finalParamsToApply.push(input);

		if (paramsExist) {
			evalMethodParams.forEach(function (param) {
				finalParamsToApply.push(param);
			});
		}

		return validationMethod.apply(null, finalParamsToApply);
	};

	this.getConstraintsFromType = function (type) {
		type = type ? type : " ";

		var constraints = ['AlwaysTrue'], //Change to AlwaysFalse in production
			Type = type instanceof Array ? type[0] : type,
			SpecLib = semanticContraints;

		//Remove @ if exists
		Type = Type ? Type.replace('@', '') : Type;

		//Does constraint list exist?
		var constraintListExists = SpecLib[Type] && SpecLib[Type].constraints;

		constraints = constraintListExists ? SpecLib[Type].constraints : constraints;

		return constraints;
	};

	this.validateType = function (input, Type) {
		Type = Type ? Type : " ";

		var constraints,
			currentType,
			listProvided = Type instanceof Array && Type.length > 1,
			remainingTypes = null,
			validationResult = true,  //turn to false with failing condition
			value = input || "";

		if (listProvided) {
			currentType = Type.shift();

			remainingTypes = Type;
			constraints = $validate.getConstraintsFromType(currentType);

		} else {
			currentType = Type;
			constraints = $validate.getConstraintsFromType(currentType);
		}

		//Test constraints for each type
		var evalResult;
		constraints.forEach(function (constraint) {
			evalResult = $validate.validateConstraint(value, constraint);

			if (validationResult && !evalResult) {

				//only update the validation result if it is not already false
				validationResult = false;
			}
		});

		//@TODO: This is WEIRD - refactor
		var firstEval = validationResult,
			finalEval = true;

		if (!firstEval) {
			return false;

		}

		if (remainingTypes) {
			remainingTypes.forEach(function (type) {
				if (!$validate.validateType(input, [type])) {
					finalEval = false;
				}
			});
		}

		return finalEval;
	};


	//Make new functions compatible with old
	validate.type = this.validateType;
	validate.data = this.validateConstraint;

	return validate;

});
