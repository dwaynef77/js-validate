define(function () {
    'use strict';
    //Works better than JS built-in isNumeric
    return function (n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    };
});