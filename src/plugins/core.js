define(function (require) {
  'use strict';

  //Works better than JS built-in isNumeric
  function __isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  return {
    Alphanumeric: function (input) {
      input = String(input);

      return /^[a-zA-Z0-9]*$/.test(input);

    },
    AlphanumericPlus: function (input, chars) {
      var regex = 'a-zA-Z0-9';

      //If chars are in an array convert to string
      chars = chars instanceof Array ?
        chars.join(' ') : chars;


      if (chars && typeof chars === 'string') {
        chars.split(' ').map(function (char) {

          //Treat all new chars as escape-worthy just in case
          regex = regex + '\\' + char;
        });

      }

      return new RegExp('^[' + regex + ']*$').test(input);
    },
    AlwaysFalse: function () {
      return false;
    },
    AlwaysTrue: function () {
      return true;
    },
    Boolean: function (input) {
      input = typeof input === 'undefined' ? '' : input.toString();
      input = input.toLowerCase();

      var acceptableBooleans = {
        'true': true,
        't': true,
        '1': true,
        'y': true,
        'yes': true,
        'false': true,
        'f': true,
        '0': true,
        'n': true,
        'no': true
      };

      return acceptableBooleans[input] ? true : false;
    },
    Integer: function () {
      var input = arguments[0] || "";

      return parseInt(input, 10) === Number(input);
    },
    Length: function(input, expectedLength) {
      return input.length === expectedLength;
    },
    Max: function (input, maxValue) {
      input = __isNumber(input) ? parseFloat(input, 10) : 0;
      maxValue = __isNumber(maxValue) ? parseFloat(maxValue, 10) : 0;

      return input <= maxValue;
    },
    MaxLen: function (input, maxLen) {
      var inputLen = input.toString().length;

      return (inputLen <= maxLen) ? true : false;
    },
    Min: function (input, minValue) {
      input = __isNumber(input) ? parseFloat(input, 10) : 0;
      minValue = __isNumber(minValue) ? parseFloat(minValue, 10) : 0;

      return input >= minValue;
    },
    MinCaps: function (input, minCaps) {
      var currChar,
        charsToCheck = input.length || 0,
        capCount = 0;

      for (var i = 0; i < charsToCheck; i++) {
        currChar = input.charAt(i);

        if (/^[A-Z]+$/.test(currChar)) {
          capCount++;
        }
      }

      return capCount >= minCaps;
    },
    MinLen: function (input, minLen) {
      var inputLen = input.toString().length;

      return (inputLen >= minLen) ? true : false;
    },
    MinNums: function (input, minNums) {
      //Enter a number with leading zero will cause fxn to break
      //075 -> will be converted to 61 so numLen will be two instead of three
      //'075' -> will be evaluated as 075 so numLen will be three

      input = input.toString();

      var inputChars = input.split(""),
        numCount = [];

      inputChars.map(function (value) {
        if (__isNumber(value)) {
          numCount.push(1);
        }
      });

      return numCount.length >= minNums;
    },
    MinSpecial: function (input, minSpec) {
      input = input.toString();

      var inputChars = input.split(""),
        specCount = [];

      //Looping a regex is not a good idea
      inputChars.map(function (value) {
        if (!__isNumber(value)) {
          if (/[^a-zA-Z]/.test(value)) {
            specCount.push(1);
          }
        }
      });

      return specCount.length >= minSpec;
    },
    Number: function (input) {
      input = typeof input === 'undefined' ? '' : input;
      input = parseInt(input, 10);

      return __isNumber(input);
    },
    Required: function (input) {
      input = typeof input === 'undefined' ? '' : input;
      input = input.toString();
      input = input.replace(' ', '');

      return input ? true : false;
    },
    RoutingNumber: function (inputToValidate) {


      var i, n = 0;

      for (i = 0; i < inputToValidate.length; i += 3) {
        n += parseInt(inputToValidate.charAt(i), 10) *
          3 + parseInt(inputToValidate.charAt(i + 1), 10) *
          7 + parseInt(inputToValidate.charAt(i + 2), 10);
      }

      // If the resulting sum is an even multiple of ten (but not zero),
      // the aba routing number is good.

      if (n !== 0 && n % 10 === 0) {
        return true;
      } else {
        return false;
      }
    },
    Text: function () {
      //@TODO: Text --> String with no character constraints?
      var input = arguments[0] || "";


      return input === String(input);
    },

    //@TODO: Remove lowerCase methods
    checked: function (params) {
      return true;
    },

    selected: function (params) {
      return true;
    },

    max: function (params) {
      return true;
    },

    min: function (params) {
      return true;
    },

    range: function (params) {
      return true;
    },

    notBlank: function (inputToValidate) {
      var input = inputToValidate.toString() || inputToValidate;

      return input.replace(/\s/g, "") !== "";
    },

    blank: function (inputToValidate) {
      var input = inputToValidate.toString() || inputToValidate;

      return input.replace(/\s/g, "") === "";
    },

    matches: function (params) {
      var result = true;

//			if ($regulaGlobal.shouldValidate(this, params)) {
//				var re;
//
//				var regex;
//				if (typeof params["regex"] === "string") {
//					regex = params["regex"].replace(/^\//, "").replace(/\/$/, "")
//				} else {
//					regex = params["regex"];
//				}
//
//				if (typeof params["flags"] !== "undefined") {
//					re = new RegExp(regex.toString().replace(/^\//, "").replace(/\/[^\/]*$/, ""), params["flags"]);
//				} else {
//					re = new RegExp(regex);
//				}
//
//				result = re.test(this.value);
//			}

      return result;
    },

    email: function (params) {
      var result = true;

//			if ($regulaGlobal.shouldValidate(this, params)) {
//				result = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i.test(this.value);
//			}

      return result;
    },

    real: function (params) {
      var result = true;

//			if ($regulaGlobal.shouldValidate(this, params)) {
//				result = /^-?([0-9]+(\.[0-9]+)?|\.[0-9]+)$/.test(this.value);
//			}

      return result;
    },

    digits: function () {

      return false;
    },

    past: function () {

      return false;
    },

    future: function () {

      return false;
    },

    url: function () {

      return false;
    }
  };

});
